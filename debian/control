Source: libkf5sane
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Sune Vuorela <sune@debian.org>,
           Maximiliano Curia <maxy@debian.org>,
           Scarlett Moore <sgmoore@kde.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               kf6-extra-cmake-modules,
               libkf5i18n-dev,
               libkf5textwidgets-dev,
               libkf5wallet-dev,
               libkf5widgetsaddons-dev,
               libksanecore1-dev,
               libsane-dev,
               libx11-dev,
               pkg-kde-tools (>= 0.12),
               qtbase5-dev
Standards-Version: 4.6.2
Homepage: https://phabricator.kde.org/source/libksane/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/libksane
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/libksane.git

Package: libkf5sane-dev
Section: libdevel
Architecture: any
Depends: libkf5sane6 (= ${binary:Version}),
         qtbase5-dev,
         ${misc:Depends}
Description: scanner library development headers
 The KDE scanner library provides an API and widgets for using scanners and
 other imaging devices supported by SANE.
 .
 This package contains the scanner library development files.

Package: libkf5sane6
Architecture: any
Depends: libkf5sane-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Provides: libkf5sane5, libkf5sane-data, libksane-data,
Replaces: libkf5sane5, libkf5sane-data, libksane-data,
Description: scanner library (runtime)
 The KDE scanner library provides an API and widgets for using scanners and
 other imaging devices supported by SANE.
 .
 This package contains the shared library.


## transitionals

Package: libkf5sane5
Architecture: all
Section: oldlibs
Depends: libkf5sane6, ${misc:Depends},
Description: Dummy transitional
 Transitional dummy package.

Package: libkf5sane-data
Architecture: all
Section: oldlibs
Depends: libkf5sane6, ${misc:Depends},
Description: Dummy transitional
 Transitional dummy package.

Package: libksane-data
Architecture: all
Section: oldlibs
Depends: libkf5sane6, ${misc:Depends},
Description: Dummy transitional
 Transitional dummy package.